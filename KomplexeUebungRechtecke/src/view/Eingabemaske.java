package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.BunteRechteckeController;
import model.Rechteck;

public class Eingabemaske extends JFrame {

	// Controller dem die eingegebenden Rechtecke hinzugefuegt werden
	private BunteRechteckeController brc;

	// Panel
	private JPanel mainPanel;
	private JPanel eingabePanel;
	private JPanel xInfo;
	private JPanel yInfo;
	private JPanel breiteInfo;
	private JPanel hoeheInfo;
	private JPanel tfXPanel;
	private JPanel tfYPanel;
	private JPanel tfBreitePanel;
	private JPanel tfHoehePanel;

	// Layout-Manager
	private GridBagLayout gbl; // Das GridBagLayout wurde gewaehlt, weil es fuer spaetere Erweiterungen der
								// Eingabemakse besser ist(so muss dann nicht das ganze Programm neugeschrieben
								// werden).
	private GridBagConstraints gbc;

	// Content
	private JLabel tfXInfo;
	private JLabel tfYInfo;
	private JLabel tfBreiteInfo;
	private JLabel tfHoeheInfo;
	private JTextField tfX;
	private JTextField tfY;
	private JTextField tfBreite;
	private JTextField tfHoehe;
	private JButton addButton;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eingabemaske frame = new Eingabemaske(new BunteRechteckeController());

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Eingabemaske(BunteRechteckeController brc) {
		// Controller auf das Attribut setzen
		this.brc = brc;
		// MainPanel
		mainPanel = new JPanel(new BorderLayout());
		// Layout-Manager
		gbl = new GridBagLayout();
		gbc = new GridBagConstraints();
		// Eingabe-Panel
		eingabePanel = new JPanel();
		eingabePanel.setLayout(gbl);
		mainPanel.add(eingabePanel, BorderLayout.CENTER);
		// Elemente des Eingabe-Panels
		xInfo = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 0.5;
		gbc.weighty = 0.5;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.ipadx = 0;
		gbc.ipady = 0;
		tfXInfo = new JLabel("X-Wert: ");
		xInfo.add(tfXInfo);
		eingabePanel.add(xInfo, gbc);

		yInfo = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		gbc.gridx = 0;
		gbc.gridy = 1;
		tfYInfo = new JLabel("Y-Wert: ");
		yInfo.add(tfYInfo);
		eingabePanel.add(yInfo, gbc);

		breiteInfo = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		gbc.gridx = 0;
		gbc.gridy = 2;
		tfBreiteInfo = new JLabel("Breite-Wert: ");
		breiteInfo.add(tfBreiteInfo);
		eingabePanel.add(breiteInfo, gbc);

		hoeheInfo = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		gbc.gridx = 0;
		gbc.gridy = 3;
		tfHoeheInfo = new JLabel("Hoehe-Wert: ");
		hoeheInfo.add(tfHoeheInfo);
		eingabePanel.add(hoeheInfo, gbc);

		// Eingabe-Felder
		tfXPanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 10, 10));
		gbc.gridx = 1;
		gbc.gridy = 0;
		tfX = new JTextField();
		tfX.setPreferredSize(new Dimension(100, 26));
		tfXPanel.add(tfX);
		eingabePanel.add(tfXPanel, gbc);

		tfYPanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 10, 10));
		gbc.gridx = 1;
		gbc.gridy = 1;
		tfY = new JTextField();
		tfY.setPreferredSize(new Dimension(100, 26));
		tfYPanel.add(tfY);
		eingabePanel.add(tfYPanel, gbc);

		tfBreitePanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 10, 10));
		gbc.gridx = 1;
		gbc.gridy = 2;
		tfBreite = new JTextField();
		tfBreite.setPreferredSize(new Dimension(100, 26));
		tfBreitePanel.add(tfBreite);
		eingabePanel.add(tfBreitePanel, gbc);

		tfHoehePanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 10, 10));
		gbc.gridx = 1;
		gbc.gridy = 3;
		tfHoehe = new JTextField();
		tfHoehe.setPreferredSize(new Dimension(100, 26));
		tfHoehePanel.add(tfHoehe);
		eingabePanel.add(tfHoehePanel, gbc);

		// Button zum Hinzufuegen der Rechtecke
		addButton = new JButton("Add");
		addButton.setPreferredSize(new Dimension(1, 40)); // nur um die Hoehe des Buttons anzupassen
		mainPanel.add(addButton, BorderLayout.SOUTH);
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				addButtonAction();
			}
		});

		// Frame

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(400, 200, 450, 300);
		setTitle("Eingabemaske");
		setContentPane(mainPanel);
		setVisible(true);
	}

	protected void addButtonAction() {
		String x = "" + tfX.getText();
		String y = "" + tfY.getText();
		String breite = "" + tfBreite.getText();
		String hoehe = "" + tfHoehe.getText();
		if (x.matches("[0-9]+") && y.matches("[0-9]+") && breite.matches("[0-9]+") && hoehe.matches("[0-9]+")) {
			Rechteck neu = new Rechteck(Integer.parseInt(x), Integer.parseInt(y), Integer.parseInt(breite),
					Integer.parseInt(hoehe));
			brc.getRechtecke().add(neu); // Rechteck dem Controller hinzufuegen
		} else {
			tfX.setText("Fehler");
			tfY.setText("Fehler");
			tfBreite.setText("Fehler");
			tfHoehe.setText("Fehler");
		}
	}
}