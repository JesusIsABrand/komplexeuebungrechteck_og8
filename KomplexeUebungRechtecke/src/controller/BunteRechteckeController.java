 package controller;
 
 import java.util.LinkedList;
 import java.util.List;
 
 import model.Rechteck;
 
 public class BunteRechteckeController {
 
 	//Attribute
 	private List<Rechteck> rechtecke;	
 
 	//Konstruktoren
 	public BunteRechteckeController() {
 		super();
 		rechtecke = new LinkedList<Rechteck>();
 	}
 
 	public BunteRechteckeController(List<Rechteck> rechtecke) {
 		super();
 		this.rechtecke = rechtecke;
 	}
 	
 	//Methoden
 	
 	//Fügt der Liste ein Rechteck hinzu
 	public void add(Rechteck rechteck){
 		rechtecke.add(rechteck);
 	}
 	
	//leert die Rechteck-Liste und erstellt die als Parameter uebergebene Anzahl Rechtecke zufaellig(im Bereich 1200x1000 Pixel).
	public void generiereZufallsRechtecke(int anzahl) {
		this.reset();
		for (int i = 0; i < anzahl; i++) {
			this.rechtecke.add(Rechteck.generiereZufallsRechteck());
		}
	}
	
 	//l�scht alle Rechtecke bzw. leert die Liste
 	public void reset(){
 		rechtecke.clear();
 	}
 	
 	@Override
 	public String toString() {
 		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
 	}
 
 	//Getter und Setter
 	public List<Rechteck> getRechtecke() {
 		return rechtecke;
 	}
 
 	public void setRechtecke(List<Rechteck> rechtecke) {
 		this.rechtecke = rechtecke;
 	}
 
 	//Main-Methode
 	public static void main(String[] args) {
 
 	}
 
 }