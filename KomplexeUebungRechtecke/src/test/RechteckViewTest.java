package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;
import view.Eingabemaske;
import view.Zeichenflaeche;

@SuppressWarnings("serial")
public class RechteckViewTest extends JFrame {

	private JPanel contentPane;

	private JMenuBar menubar;
	private JMenu menu;
	private JMenuItem menuItemAdd;
	private BunteRechteckeController brc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		new RechteckViewTest().run();
	}

	protected void run() {
		while (true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			this.revalidate();
			this.repaint();
		}

	}

	/**
	 * Create the frame.
	 */
	public RechteckViewTest() {

		this.brc = new BunteRechteckeController();
		// menue erstellen

		menubar = new JMenuBar();
		menu = new JMenu("Optionen");
		menuItemAdd = new JMenuItem("Add Rechteck");
		menuItemAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {

				menuItemAddAction();
			}
		});

		menu.add(menuItemAdd);
		menubar.add(menu);
		// Rechtecke dem Controller hinzufuegen
		brc.add(new Rechteck(330, 330, 50, 50));
		brc.add(new Rechteck(380, 380, 50, 50));
		brc.add(new Rechteck(440, 440, 50, 50));
		brc.add(new Rechteck(500, 500, 50, 50));
		brc.add(new Rechteck(560, 440, 50, 50));
		brc.add(new Rechteck(620, 380, 50, 50));
		brc.add(new Rechteck(680, 330, 50, 50));
		brc.add(new Rechteck(740, 270, 50, 50));
		brc.add(new Rechteck(800, 210, 50, 50));
		brc.add(new Rechteck(860, 150, 50, 50));
		brc.generiereZufallsRechtecke(25);
		// Frame wird charakterisiert, Menue-Leiste hinzufuegen

		setJMenuBar(menubar);
		setTitle("RechteckViewTest");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1260, 1080);
		contentPane = new Zeichenflaeche(brc);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.setVisible(true);
	}

	public void menuItemAddAction() {
		new Eingabemaske(brc);
	}

}