package test;

import model.Rechteck;
import controller.BunteRechteckeController;

public class RechteckTest {

	public static void main(String[] args) {
		
		//Rechtecke erstellen		
		//parameterloser Konstruktor
		Rechteck rechteck0 = new Rechteck();
		rechteck0.setX(10);
		rechteck0.setY(10);
		rechteck0.setBreite(30);
		rechteck0.setHoehe(40);
		
		Rechteck rechteck1 = new Rechteck();
		rechteck1.setX(25);
		rechteck1.setY(25);
		rechteck1.setBreite(100);
		rechteck1.setHoehe(20);
		
		Rechteck rechteck2 = new Rechteck();
		rechteck2.setX(260);
		rechteck2.setY(10);
		rechteck2.setBreite(200);
		rechteck2.setHoehe(100);
		
		Rechteck rechteck3 = new Rechteck();
		rechteck3.setX(5);
		rechteck3.setY(500);
		rechteck3.setBreite(300);
		rechteck3.setHoehe(25);

		Rechteck rechteck4 = new Rechteck();
		rechteck4.setX(100);
		rechteck4.setY(100);
		rechteck4.setBreite(100);
		rechteck4.setHoehe(100);
		
		//parametisierter Konstruktor
		Rechteck rechteck5 = new Rechteck(200,200,200,200);
		Rechteck rechteck6 = new Rechteck(800,400,20,20);
		Rechteck rechteck7 = new Rechteck(800,450,20,20);
		Rechteck rechteck8 = new Rechteck(850,400,20,20);
		Rechteck rechteck9 = new Rechteck(855,455,25,25);
		
		//Controller erstellen und Rechtecke hinzuf�gen
		BunteRechteckeController controller1 = new BunteRechteckeController();
		controller1.add(rechteck0);
		controller1.add(rechteck1);
		controller1.add(rechteck2);
		controller1.add(rechteck3);
		controller1.add(rechteck4);
		controller1.add(rechteck5);
		controller1.add(rechteck6);
		controller1.add(rechteck7);
		controller1.add(rechteck8);
		controller1.add(rechteck9);
		
		//Pr�fen ob Rechteckerstellung mit negativen Breiten und H�hen m�glich ist
	    Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
	    System.out.println(eck10); //Rechteck [x=-4, y=-5, breite=50, hoehe=200]
	    Rechteck eck11 = new Rechteck();
	    eck11.setX(-10);
	    eck11.setY(-10);
	    eck11.setBreite(-200);
	    eck11.setHoehe(-100);
	    System.out.println(eck11);//Rechteck [x=-10, y=-10, breite=200, hoehe=100]
		
	    //Rechteck mit zufaelligen Werten
	    Rechteck radomRechteck = Rechteck.generiereZufallsRechteck();
	    
		//Ausgabe
		System.out.println(rechteck0.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]"));
		System.out.println(controller1.toString());
		System.out.println(radomRechteck.toString());
	}

	public static boolean rechteckeTesten() {
		Rechteck[] randomRechtecke = new Rechteck[50000];
		for (int i = 0; i < randomRechtecke.length; i++) {
			randomRechtecke[i] = Rechteck.generiereZufallsRechteck();
		}
		Rechteck pruefBereich = new Rechteck(0,0,1200,1000);
		for (int i = 0; i < randomRechtecke.length; i++) {
			if (pruefBereich.enthaelt(randomRechtecke[i]) == false) 
				return false;
		}
		return true;
	}
	
}