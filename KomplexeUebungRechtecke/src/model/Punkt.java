package model;

public class Punkt {

	// Attribute
	private int x;
	private int y;

	// Konstruktoren
	public Punkt() {
		super();
		this.x = 0;
		this.y = 0;
	}

	public Punkt(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	// Getter und Setter
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	// Methode zum Vergleichen von Punkten
	public boolean equals(Punkt p) {
		if (this.getX() == p.getX() && this.getY() == p.getY()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "Punkt [x=" + x + ", y=" + y + "]";
	}

}