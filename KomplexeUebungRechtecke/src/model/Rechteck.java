package model;

import java.util.Random;

public class Rechteck {

	// Attribute
	private Punkt p;
	private int breite;
	private int hoehe;

	// Konstruktoren
	public Rechteck() {
		super();
		p = new Punkt();
		this.breite = 0;
		this.hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		p = new Punkt(x, y);
		this.setBreite(breite);
		this.setHoehe(hoehe);
	}

	// Getter und Setter
	public int getX() {
		return p.getX();
	}

	public void setX(int x) {
		this.p.setX(x);
	}

	public int getY() {
		return p.getY();
	}

	public void setY(int y) {
		this.p.setY(y);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		if (breite < 0) {
			this.breite = Math.abs(breite);
		} else {
			this.breite = breite;
		}
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		if (hoehe < 0) {
			this.hoehe = Math.abs(hoehe);
		} else {
			this.hoehe = hoehe;
		}
	}

	// Methoden

	// Methode welche ueberprueft ob ein x- und ein y-Wert zusammen in einem
	// Rechteck inhalten sind (x-Kordinate + y-Kordinate = Punkt)
	public boolean enthaelt(int x, int y) {

		if (((x <= this.getX() + this.getBreite()) && (x >= this.getX()))
				&& ((y <= this.getY() + this.getHoehe()) && (y >= this.getY()))) {
			return true;
		} else {
			return false;
		}
	}

	// Methode zur ueberpruefung ob ein Punkt in einem Rechteck ist.
	public boolean enthaelt(Punkt p) {
		return this.enthaelt(p.getX(), p.getY());
	}

	// Methode zur ueberpruefung ob ein Rechteck in einem anderen enthalten ist.
	public boolean enthaelt(Rechteck rechteck) {
		return this.enthaelt(rechteck.getX() + rechteck.getBreite(), rechteck.getY())
				&& this.enthaelt(rechteck.getX(), rechteck.getY() + rechteck.getHoehe());
	}

	/*
	 * Diese Methode generiert ein Rechteck mit zufaelligen Werten, allerdings
	 * bleibt dieses Rechteck im Bereich 1200x1000 Pixel
	 */
	public static Rechteck generiereZufallsRechteck() {
		Random r1 = new Random();
		int xRandom = r1.nextInt(1200);
		int yRandom = r1.nextInt(1000);
		int breiteRandom = r1.nextInt(1200 - xRandom);
		int hoeheRandom = r1.nextInt(1000 - yRandom);
		return new Rechteck(xRandom, yRandom, breiteRandom, hoeheRandom);
	}

	@Override
	public String toString() {
		return "Rechteck [x=" + this.p.getX() + ", y=" + this.p.getY() + ", breite=" + breite + ", hoehe=" + hoehe
				+ "]";
	}

}